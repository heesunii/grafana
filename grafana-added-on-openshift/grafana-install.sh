#!/bin/bash

NAMESPACE=myproject

# We use the project, myproject on the Minishift.
oc login -u system:admin
oc project ${NAMESPACE}


oc delete configmap grafana-datasources > /dev/null 2>&1

oc process -f grafana-template.yml -p NAMESPACE=${NAMESPACE} | oc delete -f -

PROMETHEUS_URL=$(oc get svc prometheus-k8s -n openshift-monitoring -o template --template='https://{{.metadata.name}}.{{.metadata.namespace}}.svc:9091{{println}}')
GRAFANA_TOKEN=$(oc sa get-token prometheus-k8s -n openshift-monitoring)
AUTH_PASSWORD=$(oc get secret grafana-datasources -n openshift-monitoring --template='{{ index .data "prometheus.yaml" }}' | base64 -d|grep basicAuthPassword | awk -F '"' '{print $4}')
oc process -f grafana-datasources-template.yml \
  -p PROMETHEUS_URL=${PROMETHEUS_URL} \
  -p GRAFANA_TOKEN=${GRAFANA_TOKEN} \
  -p AUTH_PASSWORD=${AUTH_PASSWORD} \
  | oc create -f -
oc process -f grafana-template.yml -p NAMESPACE=${NAMESPACE} | oc create -f -
